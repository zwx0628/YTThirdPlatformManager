//
//  PTBaseThirdPlatformManager.h
//  Plush
//
//  Created by aron on 2017/10/26.
//  Copyright © 2017年 qingot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTAbsThirdPlatformManager.h"

@interface PTBaseThirdPlatformManager : NSObject <PTAbsThirdPlatformManager>

@property (nonatomic, copy) PT_PaymentResultBlock paymentResultBlock;
@property (nonatomic, copy) PT_SignInResultBlock signInResultBlock;
@property (nonatomic, copy) PT_ShareResultBlock shareResultBlock;


// 第三方分享，子类重写该方法
- (void)doShareWithModel:(ThirdPlatformShareModel*)model;

@end
